__kernel void dfa_propagate(
        __global const float* weight_errors,
        __global const float* neuron_errors,
        const float error,
        const int neuron_width,
        const int neurons,
        __global float* write_back
        ) {
    int pos = get_global_id(0);
    //each neuron has its own neur:q
    //on_error.
    // in 1024 x 768 neuron (1024 neurons with 768 weigths and biases) 
    // there will be 1024 errors 
    int npos =  (pos - pos % neuron_width) / neurons;
    int wpos = pos % neuron_width;
    write_back[pos] = weight_errors[wpos] * neuron_errors[npos] * error;
}

__kernel void multiply(
        __global const float* a,
        __global const float* b,
        __global float* c
        ){

    int pos = get_global_id(0);
    c[pos] = a[pos] * b[pos];
}

__kernel void multiply_weights(
        __global const float* a,
        __global const float* b,
        __global float* c,
        int count,
        int max
        ){

    int pos = get_global_id(0);
    if (pos <= max) {
        int weight_pos = pos % count;
        c[pos] = a[pos] * b[weight_pos];
    }

}

__kernel void combine(
            __global const float* weights,
            int width,
            __global float* output,
            int max
        ){
    
    int pos = get_global_id(0) * width; //we want to start so far ahead
    if (pos <= max) {
        int npos = get_global_id(0);
        for (int i = 0; i < width; i++) {
            output[npos] += weights[pos+i];
        }
    }
}

