#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "network.h"
#include "data.h"

void create_context(CL* cl) {

  cl_int err;
  cl_uint platforms;
  cl_platform_id* platform_ids ;
  cl_context context = NULL;
  
  err = clGetPlatformIDs(0, NULL, &platforms);

  printf("Platforms: %d\n", platforms);
  if (err != CL_SUCCESS) {
    printf("Something went wrong with getting platform numbers\n");
  }

  platform_ids = alloca(sizeof(cl_platform_id) * platforms);

  err = clGetPlatformIDs(platforms, platform_ids, NULL);

  if (err != CL_SUCCESS) {
    printf("Something went wrong with getting platform ids\n");
  }

  //search for devices
  platforms = 0;
  cl_device_id device_id;
  err = clGetDeviceIDs(*platform_ids, CL_DEVICE_TYPE_GPU, 0, NULL, &platforms);
  printf("Devices: %d\n", platforms);
  
  if (err != CL_SUCCESS) {
    printf("Something went wrong with getting device numbers\n");
  }

  err = clGetDeviceIDs(*platform_ids, CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);

  if (err != CL_SUCCESS) {
    printf("Something went wrong with getting device ids\n");
  }

  cl_context_properties properties[] = {
    CL_CONTEXT_PLATFORM, (cl_context_properties)*platform_ids, 0
  };
  cl->device = device_id;
  cl->context = clCreateContext(properties, 1, &device_id, NULL, NULL, NULL);

  // I think I got a context now???
  
}

void create_commands(CL* cl) {
  cl_int err = 0;
  cl->commands = 0;
  cl->commands = clCreateCommandQueue(cl->context, cl->device, 0, &err);
  if (!cl->commands || err != CL_SUCCESS) {
    printf("Something went wrong building the command queue");
  }
}

void create_program(CL* cl) {
  FILE *f = fopen("cl/program.cl", "rb");
  fseek(f, 0, SEEK_END);
  long fsize = ftell(f);
  fseek(f, 0, SEEK_SET);  /* same as rewind(f); */
  //printf("Program Source Size: %ld\n",fsize);
  char *source = malloc(fsize + 1);
  fread(source, 1, fsize, f);
  fclose(f);

  source[fsize-1] = 0;

  //printf("%s\n", source);
  cl_int err = 0;
  cl->program = 0;
  cl->program = clCreateProgramWithSource(cl->context, 1, &source, NULL, &err);

  if (!cl->program || err != NULL) { 
    printf("Something went wrong with create program with source");
  }

  err = clBuildProgram(cl->program, 0, NULL, NULL, NULL, NULL);

  if (err != CL_SUCCESS)
  {

    size_t len;
    char buffer[2048];
    printf("Error: Failed to build program executable!\n");
    clGetProgramBuildInfo(cl->program, cl->device, CL_PROGRAM_BUILD_LOG,
      sizeof(buffer), buffer, &len);
    printf("%s\n", buffer);
    exit(1);
  }

  cl->dfa = clCreateKernel(cl->program, "dfa_propagate", &err);
  
  if (err!=CL_SUCCESS) 
    printf("Couldn't set dfa_propagate kernel\n");
  
  cl->mul = clCreateKernel(cl->program, "multiply", &err);

  if (err!=CL_SUCCESS) 
    printf("Couldn't set multiply kernel\n");

  cl->m_weight = clCreateKernel(cl->program, "multiply_weights", &err);

  if (err!=CL_SUCCESS)
    printf("Couldn't set multiply weight kernel\n");

  cl->comb = clCreateKernel(cl->program, "combine", &err);

  if (err!=CL_SUCCESS) 
    printf("Couldn't set combine kernel\n");

  
}

int main(int argc, char** argv) {
  
  srand(time(NULL));
  unsigned int correct = 0;

  size_t global;  //I dunno what this is for
  CL cl;

  create_context(&cl);
  create_commands(&cl);
  create_program(&cl);

	int err;
	size_t local;

	err = clGetKernelWorkGroupInfo(cl.dfa, cl.device, CL_KERNEL_WORK_GROUP_SIZE,
																 sizeof(local), &local, NULL);
	
	if (err != CL_SUCCESS) {
		printf("Couldn't retrieve kernel work group info!!!!\n");
	}

	printf("Work Groups size?: %zu\n", local);
	cl.workgroup = local;

  //start from here now xD
  //Debug
  //Grab our Data image
  Dataset* d;
  initiate_data(&d);
  //This is the Network starting
  Network n;
  init_network(&n, 3, NULL);
  add_layer(&n, 0, 1000, 784, SIGMOID);
  add_layer(&n, 1, 1000, 1000, SIGMOID);
  add_layer(&n, 2, 10, 1000, SIGMOID);
  finalise_network(&n, &cl);
  activate(&n, d->training[0].pixels);

}
