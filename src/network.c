#include "network.h"

//Important, srand() must be set
//returns a variable regular standard deviations of -3.5 and 3.5.
//It should be distributed with -1 to 1 being the most popular. 
float std_deviation_random() {

  float random = 0;

  do {
    float u1 = (rand() % 1000) / 1000.0;
    float u2 = (rand() % 1000) / 1000.0;
    float r = sqrt(-2 * log(u1));
    float theta = 2 * M_PI * u2;

    random =r * cos(theta);
  } while (isinf(random) || isnan(random) || random == 0);

  if (isinf(random) || isinf(random))
    printf("Somehow there was a mistake");

  return random;
  
}

void init_layer(Layer* l, int length, int inputs, enum ActivationType type) {
  l->length = length;
  l->width = inputs + 1; //+1 for the bias
  l->type = type;
  l->weights = malloc(sizeof(float)*l->length*l->width);
  l->multied_weights = malloc(sizeof(float)*l->length*l->width);
  for (int i = 0; i < l->length*l->width; i++) {
    l->weights[i] = std_deviation_random();
  }
  l->near_outputs = malloc(sizeof(float)*l->length);
  l->outputs = malloc(sizeof(float)*l->length);

  for (int i = 0; i < l->length;i++) {
    l->near_outputs[i] = 0;
    l->outputs[i] = 0;
  }
  
}

void init_network(Network* n, int width, FILE* output) {
  n->out = output;
  n->num = width;
  n->l = malloc(sizeof(Layer)*width);
}

void add_layer(Network* n, int pos, int length, int inputs, enum ActivationType
								type) {
	init_layer(&n->l[pos], length, inputs, type);
}

int max_floats(Network* n) {
	int max = 0;
	
	for (int i = 0; i < n->num; i++) {
		int sum = n->l[i].length * n->l[i].width;
		printf("Sum is: %d\n", sum);
		if (sum > max)
			max = sum;
	}

	return max;
}

int output_count(Network* n, int pos) {
  return n->l[pos].length;
}

void get_outputs(Layer* l, float* outputs) {
  for (int i = 0; i < l->length; i++) {
    outputs[i] = l->outputs[i];
  }
  outputs[l->length] = 1.0f;
}

int get_max_outputs(Network* n) {
  int max = 0;

  for (int i = 0; i < n->num; i++) {
    if (n->l[i].width > max) {
      max = n->l[i].width;
    }
  }

  return max + 1;

}

//Adds 1 to buffer at the end
void cpy_to_buffer(float* buffer, float* to_cpy, int length) {
  memcpy(buffer, to_cpy, length);
  buffer[length] = 1;
}

void finalise_network(Network* n, CL* cl) {
	n->cl = cl;
	int count = max_floats(n);
	int err;
	n->cl->in1 = clCreateBuffer(n->cl->context, CL_MEM_READ_ONLY, sizeof(float) *
																count, NULL, &err);

	if (err != CL_SUCCESS) {
	  printf("Something went wrong making in1\n");
	}

	n->cl->in2 = clCreateBuffer(n->cl->context, CL_MEM_READ_ONLY, sizeof(float) *
																count, NULL, &err);

  if (err != CL_SUCCESS) {
    printf("Something went wrong making in2\n");
  }
	
	n->cl->out = clCreateBuffer(n->cl->context, CL_MEM_WRITE_ONLY, sizeof(float) *
																count, NULL, &err);
  if (err != CL_SUCCESS) {
    printf("Something went wrong making out\n");
  }

	n->cl->max_memory = count;
}

void check_error(int err) {
  if (err != CL_SUCCESS) {
    printf("Oh god, running kernel failed.\n");
    switch (err) {
      case CL_INVALID_PROGRAM_EXECUTABLE:
        printf("CL_INVALID_PROGRAM_EXECUTABLE\n");
        break;
      case CL_INVALID_COMMAND_QUEUE:
        printf("CL_INVALID_COMMAND_QUEUE\n");
        break;
      case CL_INVALID_KERNEL:
        printf("CL_INVALID_KERNEL\n");
        break;
      case CL_INVALID_CONTEXT:
        printf("CL_INVALID_CONTEXT\n");
        break;
      case CL_INVALID_KERNEL_ARGS:
        printf("CL_INVALID_KERNEL_ARGS\n");
        break;
      case CL_INVALID_WORK_DIMENSION:
        printf("CL_INVALID_WORK_DIMENSION\n");
        break;
      case CL_INVALID_WORK_GROUP_SIZE:
        printf("CL_INVALID_WORK_GROUP_SIZE\n");
        break;
      case CL_INVALID_WORK_ITEM_SIZE:
        printf("CL_INVALID_WORK_ITEM_SIZE\n");
        break;
      case CL_INVALID_GLOBAL_OFFSET:
        printf("CL_INVALID_GLOBAL_OFFSET\n");
        break;
      case CL_OUT_OF_RESOURCES:
        printf("CL_OUT_OF_RESOURCES\n");
        break;
      case CL_MEM_OBJECT_ALLOCATION_FAILURE:
        printf("CL_MEM_OBJECT_ALLOCATION_FAILURE\n");
        break;
      case CL_INVALID_EVENT_WAIT_LIST:
        printf("CL_INVALID_EVENT_WAIT_LIST\n");
        break;
      case CL_OUT_OF_HOST_MEMORY:
        printf("CL_OUT_OF_HOST_MEMORY\n");
        break;
      default:
        printf("I have no idea\n");
        break;
    }
  }
}

void multiply_weights_layer(Network* n, int pos, float* inputs) {
  int err = 0;
  err = clEnqueueWriteBuffer(n->cl->commands, n->cl->in1,
                             CL_TRUE, 0, sizeof(float) * n->l[pos].length * n->l[pos].width,
                             n->l[pos].weights, 0, NULL, NULL);

  if (err != CL_SUCCESS) {
    printf("Activate Layer, IN1 failed\n");
  }

  err = clEnqueueWriteBuffer(n->cl->commands, n->cl->in2,
                             CL_TRUE, 0, sizeof(float) * (n->l[pos].width),
                             inputs, 0, NULL, NULL);

  if (err != CL_SUCCESS) {
    printf("Activate Layer, IN2 failed\n");
  }

  int width = n->l[pos].width;
  int max = width * n->l[pos].length;
  err = clSetKernelArg(n->cl->m_weight, 0, sizeof(cl_mem), &n->cl->in1);
  err |= clSetKernelArg(n->cl->m_weight, 1, sizeof(cl_mem), &n->cl->in2);
  err |= clSetKernelArg(n->cl->m_weight, 2, sizeof(cl_mem), &n->cl->out);
  err |= clSetKernelArg(n->cl->m_weight, 3, sizeof(int), &width);
  err |= clSetKernelArg(n->cl->m_weight, 4, sizeof(int), &max);

  if (err != CL_SUCCESS) {
    printf("Oh god, setting kernels failed.\n");
  }
  size_t simple = n->cl->workgroup;
  size_t range = max + (simple - (max % simple));
  printf("Range is: %zu\n", range);

  err = clEnqueueNDRangeKernel(n->cl->commands, n->cl->m_weight, 1, NULL,
                               &range, &simple, 0, NULL, NULL);

  check_error(err);

  //weight for it to finish
  err = clFinish(n->cl->commands);

  if (err != CL_SUCCESS) {
    printf("For some reason I couldn't finish.\n");
  }
  size_t new_max = max;
  err = clEnqueueReadBuffer(n->cl->commands, n->cl->out, CL_TRUE, 0, new_max,
    n->l[pos].multied_weights, 0, NULL, NULL);

  if (err != CL_SUCCESS) {
    printf("Couldn't read from the buffer....\n");
  }

  err = clFinish(n->cl->commands);

  if (err != CL_SUCCESS) {
    printf("For some reason I couldn't finish again.\n");
  }
}

void near_outputs_layer(Network* n, int pos) {
  int err = 0;

  err = clEnqueueWriteBuffer(n->cl->commands, n->cl->in1,
    CL_TRUE, 0, sizeof(float) * n->l[pos].length * n->l[pos].width,
    n->l[pos].multied_weights, 0, NULL, NULL);

  if (err != CL_SUCCESS) {
    printf("near_outputs_layer, IN1 failed\n");
  }

  int width = n->l[pos].width;
  int max = width * n->l[pos].length;
  err = clSetKernelArg(n->cl->comb, 0, sizeof(cl_mem), &n->cl->in1);
  err |= clSetKernelArg(n->cl->comb, 2, sizeof(cl_mem), &n->cl->out);
  err |= clSetKernelArg(n->cl->comb, 1, sizeof(int), &width);
  err |= clSetKernelArg(n->cl->comb, 3, sizeof(int), &max);

  if (err != CL_SUCCESS) {
    printf("Oh god, setting kernels failed.\n");
  }

  size_t simple = n->cl->workgroup;
  size_t range = n->l[pos].length + (simple - (n->l[pos].length % simple));


  err = clEnqueueNDRangeKernel(n->cl->commands, n->cl->m_weight, 1, NULL,
                               &range, &simple, 0, NULL, NULL);

  check_error(err);

  //weight for it to finish
  err = clFinish(n->cl->commands);


  err = clEnqueueReadBuffer(n->cl->commands, n->cl->out, CL_TRUE, 0,
    n->l[pos].length, n->l[pos].near_outputs, 0, NULL, NULL);

  if (err != CL_SUCCESS) {
    printf("Oh god, setting kernels failed.\n");
  }

}

void finalise_activate_layer(Network* n, int pos) {
  for (int i = 0; i < n->l[pos].length; i++) {
    n->l[pos].outputs[i] = 1/ (1 + powf(M_E, -1 * n->l[pos].near_outputs[i]));
  }
}

void activate_layer(Network* n, int pos, float* inputs) {
  multiply_weights_layer(n, pos, inputs);
  near_outputs_layer(n, pos);
  finalise_activate_layer(n, pos);

}

void activate(Network* n, float* input) {
  float* buffer = malloc(sizeof(float) * get_max_outputs(n));
  cpy_to_buffer(buffer, input, n->l[0].width);

  activate_layer(n, 0, buffer);

}


