#pragma once

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __APPLE__
	#include <OpenCL/opencl.h>
#elif __linux__
	#include <CL/opencl.h>
#endif

enum ActivationType {
  SIGMOID,
};

typedef struct {
  cl_device_id device;
  cl_context context;
  cl_command_queue commands;
  cl_program program;
  cl_kernel dfa;
  cl_kernel mul;
  cl_kernel m_weight;
  cl_kernel comb;
	size_t workgroup;
	cl_mem in1;
	cl_mem in2;
	cl_mem out;
	int max_memory;
} CL;

typedef struct {
  float* weights; //includes the bias as the last one.
  float* multied_weights;
  float* near_outputs;
  float* outputs;
  int length; //how many neurons there are
  int width; //how many weights there are, also means the inputs.
  enum ActivationType type;
} Layer;

void init_layer(Layer* l, int length, int inputs, enum ActivationType type);
void free_layer(Layer* l);


typedef struct {
  Layer* l;
  int num;
	CL* cl;
	FILE* out;
} Network;

void init_network(Network* n, int width, FILE* output);
void add_layer(Network* n, int pos, int length, int inputs, enum ActivationType
								type);
void finalise_network(Network* n, CL* cl);
void activate(Network* n, float* input);


