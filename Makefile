CC=gcc
CFLAGS= -g -Wall
NAME=network
LIB= -lm 

UNAME_S := $(shell uname -s)

ifeq ($(UNAME_S), Linux)
	LIB += -lOpenCL
endif
ifeq ($(UNAME_S), Darwin)
	LIB += -framework OpenCL
endif


network: obj/main.o obj/data.o obj/network.o
	$(CC) $(CFLAGS) -o bin/$(NAME) obj/main.o obj/data.o obj/network.o $(LIB)  

obj/main.o: src/main.c
	$(CC) $(CFLAGS) -c src/main.c -o obj/main.o

obj/network.o: src/network.c
	$(CC) $(CFLAGS) -c src/network.c -o obj/network.o

obj/data.o: src/data.c
	$(CC) $(CFLAGS) -c src/data.c -o obj/data.o

clean: 
	rm bin/$(NAME) 2> /dev/null && rm -rf obj 2> /dev/null && mkdir obj

run: network
	cd ./bin && ./$(NAME) && cd ..
